ALTER TABLE `questions` ADD `brainly_id` INT NULL AFTER `translated`, ADD UNIQUE `brainly_id_unique` (`brainly_id`);

CREATE TABLE IF NOT EXISTS `scraper_parameters` (
  `name` varchar(200) NOT NULL,
  `value` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;