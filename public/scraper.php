<?php

if (!file_exists(__DIR__.'/../src/config.php'))
	die('No config file. Rename and edit /src/config-sample.php');

require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/../vendor/pharse-master/pharse.php';

use \Curl\Curl;
use \Curl\MultiCurl;
use \App\Model\Model;

$db = new Model();

// Initialize dummy names to be used for comments
$names = require_once 'names.php';

// Initialize CURl object
$curl = new Curl();
$curl->setOpt(CURLOPT_SSL_VERIFYPEER, false);
$curl->setHeader('Content-Type', 'application/json');
$curl->setHeader('user-agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36');

$multi_curl = new MultiCurl();
$multi_curl->setOpt(CURLOPT_SSL_VERIFYPEER, false);

// data to be sent to brainly.pl  
$data = array (
  'operationName' => 'feed',
  'variables' => 
  array (
    'gradeIds' => 
    array (
    ),
    'subjectIds' => 
    array (
    ),
    'statusId' => 'ALL',
    'databaseId' => '15594474',
    'cursor' => 'Y3Vyc29yOjE1NDM3NzIyOTA=',
    'feedType' => 'PUBLIC',
  ),
  'query' => BRAINLY_GRAPHQL_FEEDS_QUERY,
);

/* Parse user parameters, */
$parameters = parseArgv($argv);

/* Get scraper parameters, next_page_token and first_page_token */
$scraper_parameters = $db->getScraperParameters();

/* If page_token is passed from the command line, then use it as a base page token, otherwise, the
** the scraper will check if there is a page_token on the database, if there is no, then it will
** scrape the page token from brainly.pl main page.
*/
$page_token = null;
if(isset($parameters['page_token'])){
	$page_token = $parameters['page_token'];
}else if(!is_null($scraper_parameters['next_page_token']) && !empty($scraper_parameters['next_page_token'])){
	$page_token = $scraper_parameters['next_page_token'];
}else{
	$page_token = scrape_first_token($curl);
}


/* If the page token is ready, start scraping. */
if(!is_null($page_token)){
	scrape_page($curl, $db, $page_token, $data);
}


function scrape_page($curl, $db, $page_token, $data){
	$data['variables']['cursor'] = $page_token;

	//
	$curl->post('https://brainly.pl/graphql/pl?op=feed', $data);	

	if ($curl->error) {
	    echo 'Error: ' . $curl->errorCode . ': ' . $curl->errorMessage . "\n";
	} else {

		$questions = json_decode($curl->rawResponse, true);

		$next_token = $questions['data']['feed']['pageInfo']['endCursor'];
		
		$db->updateScraperParameter('next_page_token', $next_token);

		$count = 0;

		$questions = $questions['data']['feed']['edges'];

		$questions_ids = array();

		foreach ($questions as $question) {

			if($db->getQuestionByBrainlyId($question['node']['databaseId']) == false){
				
				if(empty($question['node']['created']) || is_null($question['node']['created'])){
					$created = date('Y-m-d H:i:s');
				}else{
					$created = date('Y-m-d H:i:s', strtotime($question['node']['created']));
				}

				$d = ['title' => mb_convert_encoding(substr($question['node']['content'], 0, 50), 'UTF-8'), 'text' => $question['node']['content'], 'author' => $question['node']['author']['nick'], 'date_created' => $created, 'brainly_id' => $question['node']['databaseId']];

				$question_id = $db->insertQuestion($d);
				
				$answers_array = array();

				if($question_id){

					$category_slug = $question['node']['subject']['slug'];

					$category_id = false;

					$category = $db->getCategoryByName($category_slug);

					if($category == false){
						$category_id = $db->insertCategory($category_slug);
					}else{
						$category_id = $category['id'];
					}

					if($category_id){
						$db->updateQuestionCategory($question_id, $category_id);
					}

					foreach ($question['node']['answers']['nodes'] as $answer) {

						$answer_id = $db->insertAnswer(['question_id' => $question_id, 'text' => $answer['content'], 'author' => $answer['author']['nick']]);

						if($answer_id){
							$answers_array[$answer['databaseId']] = $answer_id;
						}
					}

					$questions_ids[] = ['local_id' => $question_id, 'remote_id' => $question['node']['databaseId'], 'answers' => $answers_array];
					
				}

				$count++;
			}
		}

		echo "$count questions added, next page token: " . $next_token . ", scrapping comments\n";

		scrape_comments($questions_ids);

		if(!is_null($next_token) && !empty($next_token) && $next_token !== ''){
			scrape_page($curl, $db, $next_token, $data);
		}
		
	}
}

function parseArgv($argv){
	unset($argv[0]);

	$parameters = array();

	foreach ($argv as $row) {
		$r = explode('=', $row);

		$parameters[$r[0]] = $r[1];	
	}

	return $parameters;
}

function scrape_first_token($curl){
	echo "Get first token.\n";
	$curl->get('https://brainly.pl/');

	if ($curl->error) {
	    return false;
	} else {
		$response = $curl->rawResponse;

		if (preg_match('/data-cursor-id="(.*?)"/', $response, $regs)) {
			return $regs[1];
		} else {
			return false;
		}		
				
	}	
}

function scrape_comments($questions){

	global $multi_curl;

	$multi_curl->complete(function($instance) {

	    echo 'call to "' . $instance->url . '" completed. Check comments, ' ;
	   	parse_and_save_comments($instance->response, $instance->user_data);
	   	echo "\n";
	});	

	foreach ($questions as $question) {
		$cu = $multi_curl->addGet('https://brainly.pl/zadanie/'.$question['remote_id']);
		$cu->user_data = $question;
	}

	$multi_curl->start();		
}


function parse_and_save_comments($content, $parameters){
	global $names;
	global $db;

	$comments_block = false;

	if (preg_match('%<article id="question" (.*?)</article>%s', $content, $regs)) {
		$comments_block = $regs[1];
	}

	if($comments_block){
		preg_match_all('%<span class="sg-text sg-text--break-words">(.*?)</span>%s', $comments_block, $result, PREG_PATTERN_ORDER);

		for ($i = 0; $i < count($result[0]); $i++) {
			$db->postComment(['question_id' => $parameters['local_id'], 'text' => trim(strip_tags($result[0][$i])), 'author' => $names[array_rand($names)]]);
		}
	}


	$html = \Pharse::str_get_dom($content);

	//var_dump('insert comment');

	foreach($html('div.brn-answer.js-answer') as $element) {
		if(isset($parameters['answers'][$element->attributes['data-answer-id']])){
			$local_answer_id = $parameters['answers'][$element->attributes['data-answer-id']];
			if($local_answer_id){
				foreach ($element('div.sg-list__element') as $comment) {
					$c = $comment('span.sg-text.sg-text--break-words', 0);
					if($c){
						//var_dump('answer found');
						echo "•";	
						$db->insertQuestionComments(['answer_id' => $local_answer_id, 'text' => $c->getPlainText(), 'author' => $names[array_rand($names)]]);
					}
					
				}			
			}	
		}	
	}
}