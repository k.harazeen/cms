<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitf3c01c5ba147f792629e08fba089be8f
{
    public static $files = array (
        '0e6d7bf4a5811bfa5cf40c5ccd6fae6a' => __DIR__ . '/..' . '/symfony/polyfill-mbstring/bootstrap.php',
        '3afddedb77d7801b1abe1beb8e5e6d83' => __DIR__ . '/../..' . '/src/config.php',
    );

    public static $prefixLengthsPsr4 = array (
        'T' => 
        array (
            'Twig\\' => 5,
        ),
        'S' => 
        array (
            'Symfony\\Polyfill\\Mbstring\\' => 26,
        ),
        'C' => 
        array (
            'Curl\\' => 5,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Twig\\' => 
        array (
            0 => __DIR__ . '/..' . '/twig/twig/src',
        ),
        'Symfony\\Polyfill\\Mbstring\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-mbstring',
        ),
        'Curl\\' => 
        array (
            0 => __DIR__ . '/..' . '/php-curl-class/php-curl-class/src/Curl',
        ),
    );

    public static $prefixesPsr0 = array (
        'T' => 
        array (
            'Twig_' => 
            array (
                0 => __DIR__ . '/..' . '/twig/twig/lib',
            ),
        ),
        'J' => 
        array (
            'JasonGrimes' => 
            array (
                0 => __DIR__ . '/..' . '/jasongrimes/paginator/src',
            ),
        ),
    );

    public static $classMap = array (
        'App\\Controller\\Controller' => __DIR__ . '/../..' . '/src/controller.php',
        'App\\Helper\\Helper' => __DIR__ . '/../..' . '/src/helper.php',
        'App\\Model\\Model' => __DIR__ . '/../..' . '/src/model.php',
        'Core' => __DIR__ . '/..' . '/0000dev/my/core.php',
        'Dice\\Dice' => __DIR__ . '/..' . '/rawInclude/dice.php',
        'Router' => __DIR__ . '/..' . '/rawInclude/router.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitf3c01c5ba147f792629e08fba089be8f::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitf3c01c5ba147f792629e08fba089be8f::$prefixDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInitf3c01c5ba147f792629e08fba089be8f::$prefixesPsr0;
            $loader->classMap = ComposerStaticInitf3c01c5ba147f792629e08fba089be8f::$classMap;

        }, null, ClassLoader::class);
    }
}
